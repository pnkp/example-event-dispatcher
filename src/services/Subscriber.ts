export interface Subscriber<TEvent> {
  readonly eventName: string;
  subscribe(event: TEvent): Promise<void>;
}
