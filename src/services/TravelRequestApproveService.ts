import { EventDispatcher } from './EventDispatcher';
import { TravelRequestApprovedEmailNotificationSubscriber } from './TravelRequestApprovedEmailNotificationSubscriber';
import { TravelRequestApproved } from './TravelRequestApproved';

export class TravelRequestApproveService {
  async approve(): Promise<void> {
    const eventDispatcher = new EventDispatcher();

    eventDispatcher.registerSubscribers([
      new TravelRequestApprovedEmailNotificationSubscriber(),
    ]);

    await eventDispatcher.dispatch(
      new TravelRequestApproved({ id: 1, status: 'APPROVED' }),
    );
  }
}
