import { Subscriber } from './Subscriber';
import { TravelRequestApproved } from './TravelRequestApproved';

export class TravelRequestApprovedEmailNotificationSubscriber
  implements Subscriber<TravelRequestApproved> {
  readonly eventName: string = TravelRequestApproved.name;

  subscribe(event: TravelRequestApproved): Promise<void> {
    // handle this data
    return Promise.resolve(undefined);
  }
}
