import { Subscriber } from './Subscriber';

export class EventDispatcher {
  private subscribers: Subscriber<any>[] = [];

  registerSubscribers(subscribers: Subscriber<any>[]): void {
    this.subscribers = subscribers;
  }

  async dispatch<TEvent>(event: TEvent): Promise<void> {
    await Promise.all(
      this.subscribers.map((subscriber) => {
        if (
          typeof event === 'function' &&
          subscriber.eventName === event.name
        ) {
          subscriber.subscribe(event);
        }
      }),
    );
  }
}
